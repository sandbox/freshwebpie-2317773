<?php

/**
 * Implements hook_features_export_options().
 *
 * Provides options to the feature User Interface for the module.
 */
function multi_facet_blocks_config_features_export_options() {

  $options = array();
  $query = "SELECT delta, label, custom FROM {multi_facet_block} WHERE custom = 1";
  $params = array();
  $result = db_query($query, $params);

  foreach ($result as $record) {
    $options[$record['delta']] = $record['label'];
  }

  return $options;
}


/**
 * Implements hook_features_export_options().
 *
 * Adds each selected component to the export array.
 *
 * @param $data
 * @param $export
 * @param $module_name
 * @return array
 */
function multi_facet_blocks_config_features_export($data, &$export, $module_name) {

  $export['dependencies']['multi_facet_blocks'] = 'multi_facet_blocks';

  foreach ($data as $component) {
    $export['features']['multi_facet_blocks_config'][$component] = $component;
  }

  return array();
}


/**
 * Generates the code for the components to be stored in the feature.
 *
 * @param $module_name
 * @param $data
 * @param null $export
 * @return array
 */
function multi_facet_blocks_config_features_export_render($module_name, $data, $export = NULL) {

  $code = array();
  $code[] = '  $multi_facet_blocks_configs = array();';
  $code[] = '';
  foreach ($data as $delta) {

    $item = multi_facet_block_load($delta);

    if (!empty($item)) {
      $code[] = '  $multi_facet_blocks_configs[] = ' . features_var_export($item, '  ') . ';';
    }
  }
  $code[] = '  return $multi_facet_blocks_config;';
  $code = implode("n", $code);
  return array('multi_facet_blocks_config_features_default_settings' => $code);
}


/**
 * Implements hook_features_rebuild().
 */
function multi_facet_blocks_config_features_rebuild($module) {

  $items = module_invoke($module, 'multi_facet_blocks_config_features_default_settings');

  foreach ($items as $item) {

    multi_facet_block_save($item);
  }
}


/**
 * Implements hook_features_revert().
 */
function multi_facet_blocks_config_features_revert($module) {
  multi_facet_blocks_config_features_rebuild($module);
}
