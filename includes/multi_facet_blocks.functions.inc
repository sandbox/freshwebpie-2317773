<?php

/**
 * @file
 * Contains all the custom defined functions this module uses.
 */

/**
 * Deals with building or rebuilding the multi facet block definitions
 * provided by the database and in code.
 */
function _multi_facet_blocks_rebuild() {

  // These are the most up to date definitions in code and altered in the database.
  $current_definitions = array();
  // Provide a local or condition object.
  $or = db_or();

  foreach (module_implements('multi_facet_block_definition') as $module) {

    // Ensure the definitions cache is cleared.
    cache_clear_all($module . '_multi_facet_block_definitions', 'cache');

    // Retrieve the multi facet block definitions from the current module.
    $module_definitions = module_invoke($module, 'multi_facet_block_definition');

    foreach ($module_definitions as $delta => $definition) {
      // Create a new condition to retrieve the definition from the database.
      $condition = db_and()
        ->condition('module', $module)
        ->condition('delta', $delta);
      $or->condition($condition);
      // Add unique identifiers to the definition.
      $definition['module'] = $module;
      $definition['delta'] = $delta;
      $current_definitions[$module][$delta] = $definition;
    }
  }

  if (!empty($module_definitions)) {
    // Prepare the definitions in code to allow them to be altered.
    $code_definitions = $current_definitions;
    $database_definitions = db_select('multi_facet_block', 'mfb', array('fetch' => PDO::FETCH_ASSOC))
      ->fields('mfb')
      ->condition($or)
      ->execute();
    $original_database_definitions = array();

    foreach ($database_definitions as $definition) {
      $module = $definition['module'];
      $delta = $definition['delta'];
      $original_database_definitions[$module][$delta] = $definition;
      // Ensure information that isn't in the database is kept.
      $definition['definition'] = $current_definitions[$module][$delta]['definition'];
      // Add the definition retrieved from the database to the current definitions.
      $current_definitions[$module][$delta] = $definition;
    }

    // Call drupal alter to allow hook_multi_facet_block_definition_alter calls to be executed.
    drupal_alter('multi_facet_block_definition', $current_definitions, $code_definitions);

    // Save blocks to the database if they have been updated from the original state in the database.
    foreach ($current_definitions as $module => $module_definitions) {
      // Loop through all the definitions provided by the current module.
      foreach ($module_definitions as $delta => $definition) {
        if (!empty($original_database_definitions[$module][$delta])) {
          if (array_diff_assoc($original_database_definitions[$module][$delta], $definition)) {
            drupal_write_record('multi_facet_block', $definition, array($delta));
          }
        }
      }
    }

    // Add a definitions array to the cache for each multi facet block definition.
    foreach (module_implements('multi_facet_block_definition') as $module) {
      cache_set($module . '_multi_facet_block_definitions', $current_definitions);
    }
  }
}


/**
 * Retrieves the multi facet block definition from the cache or does a rebuild
 * and then retrieves from the cache and if that does not work out then returns false.
 *
 * @param string $module The module to retrieve the definitions for.
 * @return array  An empty array if it can't retrieve definitions.
 */
function _get_multi_facet_block_definitions($module) {

  if ($cache = cache_get($module . '_multi_facet_block_definitions')) {
    return $cache->data;
  }
  else {
    _multi_facet_blocks_rebuild();
    if ($cache = cache_get($module . '_multi_facet_block_definitions')) {
      return $cache->data;
    }
    return array();
  }
}


/**
 * Loads the multi facet block handler for the given multi facet block definition.
 *
 * @param $definition
 * @return bool|object Either the handler object or
 *                     FALSE if it cannot be retrieved.
 */
function _multi_facet_blocks_load_handler($definition) {

  // Initialise the module handler as false.
  $module_handler = FALSE;
  // Get the handler for the current definition.
  $handler = $definition['definition']['handler'];
  $path = $definition['definition']['path'];

  // Try and load the handler file.
  if (file_exists($path . '/' . $handler . '.inc')) {
    require_once($path . '/' . $handler . '.inc');

    $module_handler = new $handler();
  }

  return $module_handler;
}


/**
 * Loads a multi facet block from the database.
 * This only retrieves multi facet blocks defined
 * in code.
 *
 * @param $delta
 */
function multi_facet_block_load($delta) {

  return db_query("SELECT * FROM {multi_facet_block} WHERE delta = :delta AND custom = 1",
                  array(':delta' => $delta))->fetchAssoc();
}


/**
 * Save a multi facet block to the database.
 *
 * @param $multi_facet_block
 * @return bool|int
 */
function multi_facet_block_save($multi_facet_block) {

  return drupal_write_record('multi_facet_block', $multi_facet_block, array($multi_facet_block->delta));
}

/**
 * Retrieves the enabled facets for a given environment
 * for the block realm.
 *
 * @param string $environment_id  The environment to retrieve the facets for.
 * @return array  The enabled facets for the given realm.
 */
function multi_facet_blocks_get_enabled_facets($environment_id = '') {

  $enabled_facets = array();
  // Retrieve the default searcher if one isn't available.
  $environment_id = $environment_id ? $environment_id : apachesolr_default_environment();
  $searcher = 'apachesolr@' . $environment_id;
  // Instantiate the adapter and load the realm.
  $adapter = facetapi_adapter_load($searcher);
  $realm = facetapi_realm_load('block');

  $facets = facetapi_get_facet_info($searcher);

  foreach ($facets as $facet_name => $facet) {
    $settings = $adapter->getFacetSettings($facet, $realm);
    if ($settings->enabled) {
      $enabled_facets[$facet_name] = $facet['label'];
    }
  }

  return $enabled_facets;
}


/**
 * Loads the machine names or keys of all the solr environments available.
 * @return array
 */
function multi_facet_blocks_load_solr_environment_keys() {

  $new_environments = array();
  $environments = array_keys(apachesolr_load_all_environments());
  foreach ($environments as $environment) {
    $new_environments[$environment] = $environment;
  }
  return $new_environments;
}
