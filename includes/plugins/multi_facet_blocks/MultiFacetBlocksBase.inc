<?php

/**
 * @file
 * Contains the base class holding the core functionality for multi facet blocks.
 */

/**
 * The base/default class for multi-facet blocks.
 *
 * This provides the fundamental handlers to deal with theme functions,
 * ajax callbacks, the block definition, any preprocessing that is needed
 * before render and required apache solr information.
 *
 * Class MultiFacetBlocksBase
 */
class MultiFacetBlocksBase {

  /**
   * Provides and returns a definition of the block
   * to be fed to the Drupal core block api.
   *
   * @param string $delta The id to be used for the facet block definition.
   * @return array  A definition for the block to be registered with the Block API.
   */
  function blockDefinition($delta = '', $label = '', $cache = DRUPAL_NO_CACHE) {
    return array(
      'delta' => $delta,
      'info' => t('Multi Facet Block : ' . $label),
      'cache' => $cache,
    );
  }

  /**
   * Carries out ajax page reload for facet blocks items.
   *
   * Deals with reloading the facet block based on user selection.
   */
  function ajaxCallback() {

  }

  /**
   * Provides information for the solr search environment.
   */
  function solrSearchInfo() {
    return array(
      'environment' => '',
    );
  }

  function preprocessBlock($vars) {

  }

  function themeCallback($vars) {

  }

  function themeInfo() {

  }

  function presave() {


  }
}
