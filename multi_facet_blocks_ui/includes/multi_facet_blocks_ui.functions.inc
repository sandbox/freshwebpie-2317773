<?php

/**
 * The main config page for multi facet blocks.
 */
function multi_facet_blocks_ui_main_page() {

  $output = "<ul class=\"action-links\"><li>";
  // Add action link for adding new multi facet blocks.
  $output .= l('New multi facet block', 'admin/config/search/multi_facet_blocks/add');
  // Close the action links section.
  $output .= "</li></ul>";
  // Embed the multi facet blocks view.
  $output .= views_embed_view('multi_facet_block_config', 'multi_facet_block_config');

  return $output;
}


/**
 * Deals with rendering the title based on the current multi-facet
 * block's human readable name.
 */
function multi_facet_blocks_ui_title_callback($multi_facet_block) {

  return 'Edit ' . $multi_facet_block->label;
}


/**
 * @param bool $multi_facet_block
 * @return array|mixed
 */
function multi_facet_blocks_ui_add_edit_page($multi_facet_block = FALSE) {
  if ($multi_facet_block)
    return drupal_get_form('multi_facet_blocks_ui_add_edit_form', $multi_facet_block);
  else
    return drupal_get_form('multi_facet_blocks_ui_add_edit_form');
}


/**
 * The add and edit form for multi facet blocks.
 */
function multi_facet_blocks_ui_add_edit_form($form, &$form_state, $multi_facet_block = FALSE) {

  // Add the facet block entity to the form state.
  if ($multi_facet_block)
    $form_state['store']['#multi_facet_block'] = $multi_facet_block;

  $form['delta'] = array(
    '#type' => 'textfield',
    '#title' => t('Delta'),
    '#description' => t('Unique machine name for the facet block, must be all lower case with underscores.
                        (e.g. muti_facet_block_home_page)'),
    '#required' => TRUE,
    '#element_validate' => array('_multi_facet_blocks_ui_add_edit_delta_validate'),
    '#default_value' => $multi_facet_block ? $multi_facet_block->delta : '',
    '#disabled' => $multi_facet_block ? TRUE : FALSE,
  );

  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label'),
    '#description' => t('Human readable label for the facet block'),
    '#required' => TRUE,
    '#default_value' => $multi_facet_block ? $multi_facet_block->label : '',
  );

  $form['theme'] = array(
    '#type' => 'textfield',
    '#title' => t('Theme'),
    '#description' => t('The key of the theme renderer to be used.'),
    '#required' => TRUE,
    //'#element_validate' => array('_multi_facet_blocks_ui_add_edit_theme_validate'),
    '#default_value' => $multi_facet_block ? $multi_facet_block->theme : '',
  );

  $form['handler'] = array(
    '#type' => 'textfield',
    '#title' => t('Handler'),
    '#description' => t('The handler class that deals with ajax callbacks,
                         providing a block api definition and other tasks all within a single class.'),
    '#required' => TRUE,
    '#element_validate' => array('_multi_facet_blocks_ui_add_edit_handler_validate'),
    '#default_value' => $multi_facet_block ? $multi_facet_block->handler : '',
  );

  $form['module'] = array(
    '#type' => 'textfield',
    '#title' => t('Module'),
    '#description' => t('Module providing the handler.'),
    '#required' => TRUE,
    '#element_validate' => array('_multi_facet_blocks_ui_add_edit_module_validate'),
    '#default_value' => $multi_facet_block ? $multi_facet_block->module : '',
  );

  $form['path'] = array(
    '#type' => 'textfield',
    '#title' => t('Path'),
    '#description' => t('The path to the handler relative to the specified module (Use [root] for the module\'s root path)'),
    '#required' => TRUE,
    '#element_validate' => array('_multi_facet_blocks_ui_add_edit_path_validate'),
    '#default_value' => $multi_facet_block ? $multi_facet_block->path : '',
  );

  $env_keys = multi_facet_blocks_load_solr_environment_keys();
  reset($env_keys);
  $default_key = key($env_keys);
  $form['environment_id'] = array(
    '#type' => 'select',
    '#title' => t('Search Environment Id'),
    '#description' => t('The Apache solr environment this multi facet block should be used in.'),
    '#required' => TRUE,
    '#options' => $env_keys,
    '#default_value' => $multi_facet_block ? $multi_facet_block->environment_id : $env_keys[$default_key],
    '#ajax' => array(
      'event' => 'change',
      'callback' => 'multi_facet_blocks_ui_env_id_ajax_callback',
      'wrapper' => 'multi_facet_block_facet_options',
    ),
  );

  if (!empty($form_state['values']['environment_id']))
    $env_id = $form_state['values']['environment_id'];
  else
    $env_id = $form['environment_id']['#default_value'];

  $form['facets'] = array(
    '#type' => 'container',
    '#id' => 'multi_facet_block_facet_options',
  );

  $facets = multi_facet_blocks_get_enabled_facets($env_id);
  $form['facets']['options'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Facets'),
    '#description' => t('Choose the facets that should be used in this multi facet block.'),
    '#options' => $facets,
  );

  $current_path_alias = $multi_facet_block ?
                        MULTI_FACET_BLOCKS_EDIT_PAGE_PREFIX . $multi_facet_block->delta :
                        MULTI_FACET_BLOCKS_ADD_PAGE;
  $environment_facets_path = '/admin/config/search/apachesolr/settings/' . check_plain($env_id) .
    '/facets?destination=' . $current_path_alias;

  if (!empty($facets)) {
    $form['facets']['manage_facets'] = array(
      '#type' => 'markup',
      '#markup' => t('<a href="@url">Manage facets</a>', array('@url' => $environment_facets_path)),
    );
  }

  if (empty($facets)) {
    $form['facets']['facets_not_enabled'] = array(
      '#type' => 'markup',
      '#markup' => "<p>No facets could be found for the selected environment,
                    <br/> either select a different environment or enable facets
                    <a href=\"{$environment_facets_path}\">here</a></p>",
    );
  }
  else if (isset($form['facets_not_enabled']))
    unset($form['facets_not_enabled']);

  $form['actions'] = array('#type' => 'actions');

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  $form['#submit'][] = 'multi_facet_blocks_ui_add_edit_submit';

  return $form;
}


/**
 * Callback to update the facets section on select of search environment.
 */
function multi_facet_blocks_ui_env_id_ajax_callback($form, &$form_state) {

  return $form['facets'];
}


/**
 * Validation callback for ensuring that the path to the handler callback
 * is of the correct format.
 */
function _multi_facet_blocks_ui_add_edit_path_validate($element, &$form_state, $form) {

  if ($element['#value'] != MULTI_FACET_BLOCKS_UI_MODULE_ROOT_SHORTCODE) {
    if (!preg_match("/^([A-Za-z0-9]|\\/)+$/", $element['#value'])) {
      form_error($element, t('You must enter a valid path from the module root.'));
    }
  }
}


/**
 * Validation callback for ensuring the delta field is in the standardised machine
 * name format.
 */
function _multi_facet_blocks_ui_add_edit_delta_validate($element, &$form_state, $form) {

  if (!preg_match("/^([a-z0-9]|_)+$/", $element['#value'])) {
    form_error($element, t('Delta can only be lower case letters, underscores and digits.'));
  }
}


/**
 * Validation callback for ensuring the specified module exists.
 */
function _multi_facet_blocks_ui_add_edit_module_validate($element, &$form_state, $form) {

  if (!preg_match("/^([a-z0-9]|_)+$/", $element['#value']))
    form_error($element, t('You must enter a legitimate and enabled module machine name.'));
  else if (!module_exists($element['#value']))
    form_error($element, t('The specified module either does not exist or is not enabled.'));
}


/**
 * Validation callback to ensure the handler is in the correct format for
 * a legitimate class definition name.
 */
function _multi_facet_blocks_ui_add_edit_handler_validate($element, &$form_state, $form) {

  if (!preg_match("/^[A-Za-z]([a-zA-Z0-9]|_)+$/", $element['#value']))
    form_error($element, t('Handler must be a legitimate class name in upper camel case. (e.g. HomepageMultiFacetsBlock)'));
}


/**
 * Submit handler in which the multi facet block is saved.
 */
function multi_facet_blocks_ui_add_edit_submit($form, &$form_state) {

  if (!empty($form_state['store']['#multi_facet_block']))
    $multi_facet_block = $form_state['store']['#multi_facet_block'];
  else
    $multi_facet_block = new stdClass;

  $multi_facet_block->delta = $form_state['values']['delta'];
  $multi_facet_block->label = $form_state['values']['label'];
  $multi_facet_block->theme = $form_state['values']['theme'];
  $multi_facet_block->handler = $form_state['values']['handler'];
  $multi_facet_block->module = $form_state['values']['module'];
  $multi_facet_block->path = $form_state['values']['path'];
  $multi_facet_block->environment_id = $form_state['values']['environment_id'];
  multi_facet_block_save($multi_facet_block);
}
