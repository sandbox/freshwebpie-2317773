<?php

/**
 * Implements hook_views_data().
 */
function multi_facet_blocks_ui_views_data() {

  return array(
    'multi_facet_block' => array(
      'table' => array(
        'group' => t('Multi Facet Block'),
        'base' => array(
          'field' => 'delta',
          'title' => 'Multi Facet Block',
          'help' => 'Multi Facet Block database table.',
        ),
      ),
      'delta' => array(
        'title' => t('Delta'),
        'help' => t('Multi facet block machine name.'),
        // Set delta to sortable.
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_string',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
        'argument' => array(
          'handler' => 'views_handler_argument_string',
        ),
      ),
      'label' => array(
        'title' => t('Label'),
        'help' => t('Multi facet block human readable name.'),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_string',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
        'argument' => array(
          'handler' => 'views_handler_argument_string',
        ),
      ),
    ),
  );
}
