<?php

/**
 * @file
 * Provides examples of usage for the multi_facet_blocks module's api.
 */

/**
 * Allows modules to define multi facet blocks.
 *
 * This should be added to the module's main .module file
 * or an include for block/facet functionality such as .blocks.inc.
 *
 * This hook allows you to define the basic information
 * for multi facet blocks. This includes the handler to be used
 * to generate the block content and deal with the functionality
 * in preparation and in responding to ajax calls.
 *
 * @return array  An array of multi facet block definitions.
 *   'handler' : The class name of the handler to deal with the heavy lifting.
 *   'path' : The path to the directory containing the handler.
 *   'facets' : An array of facet machine names to be provided to the handler.
 */
function hook_multi_facet_block_definition() {

  $definitions = array();

  $definitions['example_module_blog_search_block'] = array(
    'label' => t('Example module blog search block'),
    'handler' => 'ExampleModuleBlogSearchBlock',
    'path' => drupal_get_path('module', 'example_module') . '/includes',
    'search_info' => array(
      'environment_id' => 'default_solr',
    ),
    'theme' => 'example_module_blog_search_block',
    'facets' => array(
      'ftm_price',
      'sm_description_text',
      'ftm_size',
    ),
  );

  return $definitions;
}


/**
 * Allows modules to override default multi facet block definitions.
 *
 * @param array $definitions  The finalised definitions to be altered.
 * @param array $code_definitions The definitions provided in code by various modules.
 */
function hook_multi_facet_block_definition_alter(&$definitions, $code_definitions) {

  $example_definition = &$definitions['example_module_blog_search_block'];
  $example_definition['handler'] = 'MyCustomExampleModuleBlogSearchBlock';
  $example_definition['facets'][] = 'ftm_colour';
}
