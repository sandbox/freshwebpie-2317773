--------------------------------------------------------------------------------------------------
Multi-Facet Blocks
--------------------------------------------------------------------------------------------------

This module provides an interface for both developers and site builders to easily create re-usable
collections of Facet items provided by the Facet API and Apache Solr modules.

If you come across any issues or bugs with this module post it as a new issue at the project's
create new issue page [1].

[1] https://www.drupal.org/node/add/project-issue/multi_facet_blocks

Contents:
  - Overview
  - Developer information
  - User information

-------------------------------------------
Overview
-------------------------------------------

After coming across a few projects where advanced faceted search needs were rising up I found that a lot of messy,
not very flexible or re-usable code was needed to create custom multi-facet block implementations.

By design the Facet API module assigns each facet it's own block with Facet API bonus allowing the facet blocks
dependency on each-other.

-------------------------------------------
Developer information
-------------------------------------------

-------------------------------------------
User information
-------------------------------------------